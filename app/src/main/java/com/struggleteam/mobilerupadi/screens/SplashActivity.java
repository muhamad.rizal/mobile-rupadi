package com.struggleteam.mobilerupadi.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.struggleteam.mobilerupadi.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Thread(() -> {
            try{
                Thread.sleep(3000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }finally {
                runOnUiThread(() -> {
                    startActivity(new Intent(getApplicationContext(), OnBoardingActivity.class));
                    finish();
                });
            }
        }).start();
    }
}
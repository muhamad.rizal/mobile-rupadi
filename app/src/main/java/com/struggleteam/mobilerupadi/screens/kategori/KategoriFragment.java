package com.struggleteam.mobilerupadi.screens.kategori;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.models.home.Banner;
import com.struggleteam.mobilerupadi.models.kategori.Kategori;
import com.struggleteam.mobilerupadi.models.kategori.KategoriItem;
import com.struggleteam.mobilerupadi.screens.belanja.BelanjaFragment;
import com.struggleteam.mobilerupadi.utils.ApiUtils;
import com.struggleteam.mobilerupadi.utils.MobileService;
import com.struggleteam.mobilerupadi.utils.Static;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KategoriFragment extends Fragment {

    RecyclerView recycle_kategori;
    LinearLayout layout_kategori;
    MobileService mobileService;
    CircularProgressIndicator progress_kategori;

    ArrayList<Kategori> ListKategori = new ArrayList<>();
    private static final String TAG = "KategoriFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_kategori, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progress_kategori = view.findViewById(R.id.progress_kategori);
        layout_kategori = view.findViewById(R.id.layout_kategori);
        recycle_kategori = view.findViewById(R.id.recycle_kategori);
        mobileService = ApiUtils.MobileService(getContext());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recycle_kategori.setLayoutManager(gridLayoutManager);

        loadKategori();
    }

    private void loadKategori() {
        progress_kategori.setVisibility(View.VISIBLE);
        Call<KategoriItem> kategoriItem = mobileService.getkategori();
        kategoriItem.enqueue(new Callback<KategoriItem>() {
            @Override
            public void onResponse(Call<KategoriItem> call, Response<KategoriItem> response) {
                KategoriItem body = response.body();
                if (response.isSuccessful()){
                    if (!body.isState()){
                        initializeKategori();
                        ArrayList<Kategori> kategoriList = new ArrayList<>();
                        KategoriAdapter kategoriAdapter = new KategoriAdapter(kategoriList);

                        recycle_kategori.setAdapter(kategoriAdapter);
                        layout_kategori.setVisibility(View.GONE);
                        progress_kategori.setVisibility(View.GONE);
                    } else {
                        Log.d(TAG, "kategori data: " + body.getData());
                        initializeKategori();
                        for (int i = 0; i < body.getData().size(); i++){
                            final Kategori itemKategori = new Kategori();
                            itemKategori.setId(body.getData().get(i).getId());
                            itemKategori.setFoto_kategori(body.getData().get(i).getFoto_kategori());
                            itemKategori.setKategori(body.getData().get(i).getKategori());
                            ListKategori.add(itemKategori);
                        }

                        KategoriAdapter kategoriAdapter = new KategoriAdapter(ListKategori);
                        recycle_kategori.setAdapter(kategoriAdapter);
                        layout_kategori.setVisibility(View.VISIBLE);

                        progress_kategori.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<KategoriItem> call, Throwable t) {
                Log.d(TAG, "gagal: " + t.toString());
                progress_kategori.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Gagal load kategori! Coba lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeKategori() {
        ListKategori = new ArrayList<>();
        KategoriAdapter kategoriAdapter = new KategoriAdapter(ListKategori);
        recycle_kategori.setAdapter(kategoriAdapter);
        ListKategori.clear();
    }

    class KategoriAdapter extends RecyclerView.Adapter<KategoriAdapter.KategoriHolder> {

        ArrayList<Kategori> kategoriItem;

        public KategoriAdapter(ArrayList<Kategori> kategoriItem) {
            this.kategoriItem = kategoriItem;
        }

        @NonNull
        @Override
        public KategoriHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_kategori, parent, false);
            return new KategoriAdapter.KategoriHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull KategoriHolder holder, int position) {
            String fotoBanner = Static.STORAGE_BANNER + kategoriItem.get(position).getFoto_kategori();
            Picasso.get().load(fotoBanner)
                    .centerCrop()
                    .placeholder(R.drawable.item_background)
                    .fit()
                    .into(holder.item_gambar_kategori);
            holder.item_nama_kategori.setText(kategoriItem.get(position).getKategori());
        }

        @Override
        public int getItemCount() {
            return kategoriItem.size();
        }


        public class KategoriHolder extends RecyclerView.ViewHolder {
            ImageView item_gambar_kategori;
            TextView item_nama_kategori;
            public KategoriHolder(@NonNull View itemView) {
                super(itemView);
                item_gambar_kategori = itemView.findViewById(R.id.gambar_baju_kategori);
                item_nama_kategori = itemView.findViewById(R.id.text_baju_kategori);
            }
        }
    }
}
package com.struggleteam.mobilerupadi.screens.profil;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.material.button.MaterialButton;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.models.user.User;
import com.struggleteam.mobilerupadi.screens.LoginActivity;
import com.struggleteam.mobilerupadi.screens.RegisterActivity;
import com.struggleteam.mobilerupadi.utils.ApiUtils;
import com.struggleteam.mobilerupadi.utils.MobileService;
import com.struggleteam.mobilerupadi.utils.Preferences;

public class ProfilFragment extends Fragment {

    MaterialButton btnLogin, btnDaftar;
    MobileService mobileService;
    LinearLayout layout_informasi;
    RelativeLayout layout_main;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profil, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layout_informasi = view.findViewById(R.id.layout_informasi);
        layout_main = view.findViewById(R.id.content_profil);

        btnLogin = view.findViewById(R.id.btnLoginProfil);
        btnDaftar = view.findViewById(R.id.btnDaftarProfil);
        mobileService = ApiUtils.MobileService(getContext());

        checkauthentication();

        btnLogin.setOnClickListener(v -> startActivity(new Intent(getActivity(), LoginActivity.class)));
        btnDaftar.setOnClickListener(v -> startActivity(new Intent(getActivity(), RegisterActivity.class)));
    }

    private void checkauthentication() {
        User user = Preferences.getUser(getContext());

        if (user == null){
            layout_main.setVisibility(View.GONE);
            layout_informasi.setVisibility(View.VISIBLE);
        } else{
            layout_main.setVisibility(View.VISIBLE);
            layout_informasi.setVisibility(View.GONE);
        }
    }
}
package com.struggleteam.mobilerupadi.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.struggleteam.mobilerupadi.R;

public class RegisterActivity extends AppCompatActivity {

    TextView text_login_daftar;
    TextInputEditText et_username, et_email, et_pass, et_phone;
    Button btn_daftar;
    MaterialButton daftar_google;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        text_login_daftar = findViewById(R.id.text_login_daftar);
        et_username = findViewById(R.id.et_user_daftar);
        et_email = findViewById(R.id.et_email_daftar);
        et_pass = findViewById(R.id.et_pass_daftar);
        et_phone = findViewById(R.id.et_phone_daftar);

        btn_daftar = findViewById(R.id.btn_daftar);
        daftar_google = findViewById(R.id.btn_google_daftar);
    }
}
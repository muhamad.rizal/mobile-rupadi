package com.struggleteam.mobilerupadi.screens.edukasi;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.models.edukasi.Edukasi;
import com.struggleteam.mobilerupadi.models.edukasi.EdukasiItem;
import com.struggleteam.mobilerupadi.models.home.Barang;
import com.struggleteam.mobilerupadi.models.home.BarangItem;
import com.struggleteam.mobilerupadi.screens.belanja.BelanjaFragment;
import com.struggleteam.mobilerupadi.utils.ApiCovid;
import com.struggleteam.mobilerupadi.utils.ApiUtils;
import com.struggleteam.mobilerupadi.utils.DataIndonesia;
import com.struggleteam.mobilerupadi.utils.MobileService;
import com.struggleteam.mobilerupadi.utils.Static;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EdukasiFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "EdukasiFragment";

    LinearLayout layout_kasus;
    TextView sembuh, meninggal, positif;
    CircularProgressIndicator progress_covid, progress_edukasi;
    RecyclerView recycle_edukasi;
    SwipeRefreshLayout refresh_edukasi;
    MobileService mobileService;

    ArrayList<Edukasi> ListEdukasi = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edukasi, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mobileService = ApiUtils.MobileService(getContext());
        layout_kasus = view.findViewById(R.id.linear_kasus_edukasi);
        sembuh = view.findViewById(R.id.text_sembuh_edukasi);
        meninggal = view.findViewById(R.id.text_meninggal_edukasi);
        positif = view.findViewById(R.id.text_positif_edukasi);
        recycle_edukasi = view.findViewById(R.id.recycle_edukasi);

        progress_edukasi = view.findViewById(R.id.progress_edukasi);
        progress_covid = view.findViewById(R.id.progress_covid_edukasi);
        refresh_edukasi = view.findViewById(R.id.layout_refresh_kategori);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recycle_edukasi.setLayoutManager(layoutManager);

        refresh_edukasi.setOnRefreshListener(this);

        loadCovid();
        loadEdukasi();
    }

    private void loadCovid() {
        progress_covid.setVisibility(View.VISIBLE);

        Call<List<DataIndonesia>> dataCovidIndonesia = ApiCovid.service().getKasus();
        dataCovidIndonesia.enqueue(new Callback<List<DataIndonesia>>() {
            @Override
            public void onResponse(Call<List<DataIndonesia>> call, Response<List<DataIndonesia>> response) {
                progress_covid.setVisibility(View.GONE);
                layout_kasus.setVisibility(View.VISIBLE);

                sembuh.setText(response.body().get(0).getSembuh());
                meninggal.setText(response.body().get(0).getMeninggal());
                positif.setText(response.body().get(0).getPositif());
            }

            @Override
            public void onFailure(Call<List<DataIndonesia>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.toString());
                progress_covid.setVisibility(View.GONE);
                layout_kasus.setVisibility(View.VISIBLE);

                sembuh.setText("-");
                meninggal.setText("-");
                positif.setText("-");

                Toast.makeText(getContext(), "Gagal load data kasus! Silahkan coba lagi.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadEdukasi() {
        progress_edukasi.setVisibility(View.VISIBLE);

        Call<EdukasiItem> edukasiItem = mobileService.getedukasi();
        edukasiItem.enqueue(new Callback<EdukasiItem>() {
            @Override
            public void onResponse(Call<EdukasiItem> call, retrofit2.Response<EdukasiItem> response) {
                EdukasiItem body = response.body();
                if (response.isSuccessful()){
                    if (!body.isState()){
                        initializeEdukasi();
                        ArrayList<Edukasi> EdukasiList = new ArrayList<>();
                        EdukasiAdapter edukasiAdapter = new EdukasiAdapter(EdukasiList);

                        recycle_edukasi.setAdapter(edukasiAdapter);
                        recycle_edukasi.setVisibility(View.GONE);
                        progress_edukasi.setVisibility(View.GONE);
                    } else {
                        Log.d(TAG, "response data:  " + body.getData());
                        initializeEdukasi();
                        for (int i = 0; i < body.getData().size(); i++){
                            final Edukasi itemEdukasi = new Edukasi();
                            itemEdukasi.setFoto_blog(body.getData().get(i).getFoto_blog());
                            itemEdukasi.setJudul(body.getData().get(i).getJudul());
                            itemEdukasi.setHeadline(body.getData().get(i).getHeadline());
                            itemEdukasi.setDescription(body.getData().get(i).getDescription());
                            itemEdukasi.setCreated_at(body.getData().get(i).getCreated_at());
                            ListEdukasi.add(itemEdukasi);
                        }

                        EdukasiAdapter edukasiAdapter = new EdukasiAdapter(ListEdukasi);
                        recycle_edukasi.setAdapter(edukasiAdapter);
                        recycle_edukasi.setVisibility(View.VISIBLE);
                        progress_edukasi.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<EdukasiItem> call, Throwable t) {
                Log.d(TAG, "gagal: " + t.toString());
                progress_edukasi.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Gagal load edukasi! Coba lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeEdukasi() {
        ListEdukasi = new ArrayList<>();
        EdukasiAdapter edukasiAdapter = new EdukasiAdapter(ListEdukasi);
        recycle_edukasi.setAdapter(edukasiAdapter);
        ListEdukasi.clear();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(() -> {
            initializeEdukasi();
            loadCovid();
            loadEdukasi();
            refresh_edukasi.setRefreshing(false);
        }, 2000);
    }

    class EdukasiAdapter extends RecyclerView.Adapter<EdukasiAdapter.EdukasiHolder> {

        ArrayList<Edukasi> edukasiItem;

        public EdukasiAdapter(ArrayList<Edukasi> edukasiItem) {
            this.edukasiItem = edukasiItem;
        }

        @NonNull
        @Override
        public EdukasiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_edukasi, parent, false);
            return new EdukasiHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull EdukasiHolder holder, int position) {
            String fotoBlog = Static.STORAGE_BLOG + edukasiItem.get(position).getFoto_blog();
            Picasso.get().load(fotoBlog)
                    .centerCrop()
                    .fit()
                    .placeholder(R.drawable.produk_item)
                    .into(holder.item_gambar_edukasi);

            Log.d(TAG, "headline: " + edukasiItem.get(position).getHeadline());
            holder.nama_edukasi.setText(edukasiItem.get(position).getJudul());
            holder.keterangan_edukasi.setText(edukasiItem.get(position).getHeadline());
            holder.date_edukasi.setText(edukasiItem.get(position).getCreated_at());
        }

        @Override
        public int getItemCount() {
            return edukasiItem.size();
        }

        class EdukasiHolder extends RecyclerView.ViewHolder {
            TextView nama_edukasi, keterangan_edukasi, date_edukasi, lihat_detail;
            ImageView item_gambar_edukasi;

            EdukasiHolder(@NonNull View itemView) {
                super(itemView);
                item_gambar_edukasi = itemView.findViewById(R.id.item_gambar_edukasi);
                nama_edukasi = itemView.findViewById(R.id.item_nama_edukasi);
                keterangan_edukasi = itemView.findViewById(R.id.item_keterangan_edukasi);
                date_edukasi = itemView.findViewById(R.id.item_tanggal_edukasi);
                lihat_detail = itemView.findViewById(R.id.item_lihat_detail_edukasi);
            }
        }
    }
}
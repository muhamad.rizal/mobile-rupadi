package com.struggleteam.mobilerupadi.screens.belanja;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.faltenreich.skeletonlayout.SkeletonLayout;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.squareup.picasso.Picasso;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.models.home.Banner;
import com.struggleteam.mobilerupadi.models.home.BannerItem;
import com.struggleteam.mobilerupadi.models.home.Barang;
import com.struggleteam.mobilerupadi.models.home.BarangItem;
import com.struggleteam.mobilerupadi.utils.ApiUtils;
import com.struggleteam.mobilerupadi.utils.MobileService;
import com.struggleteam.mobilerupadi.utils.Static;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BelanjaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    TextView lihat_semua;
    SkeletonLayout skeleton_belanja;
    RecyclerView recycle_produk, recycle_banner;
    MobileService mobileService;
    CircularProgressIndicator progress_belanja, progress_banner;
    SwipeRefreshLayout swipe_belanja;

    ArrayList<Barang> ListBarang = new ArrayList<>();
    ArrayList<Banner> ListBanner = new ArrayList<>();

    private static final String TAG = "BelanjaFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_belanja, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lihat_semua = view.findViewById(R.id.text_lihat_semua_belanja);
        skeleton_belanja = view.findViewById(R.id.layout_skeleton_belanja);
        progress_belanja = view.findViewById(R.id.progress_belanja);
        progress_banner = view.findViewById(R.id.progress_banner_belanja);

        recycle_banner = view.findViewById(R.id.recycle_banner);
        recycle_produk = view.findViewById(R.id.recycle_produk);

        swipe_belanja = view.findViewById(R.id.layout_refresh_belanja);
        mobileService = ApiUtils.MobileService(getContext());

        RecyclerView.LayoutManager layoutBanner = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        recycle_banner.setLayoutManager(layoutBanner);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false);
        recycle_produk.setLayoutManager(layoutManager);

        swipe_belanja.setOnRefreshListener(this);

        loadBarang();
        loadBanner();
    }

    private void loadBanner() {
        progress_banner.setVisibility(View.VISIBLE);
        Call<BannerItem> bannerItem = mobileService.getbanner();
        bannerItem.enqueue(new Callback<BannerItem>() {
            @Override
            public void onResponse(Call<BannerItem> call, Response<BannerItem> response) {
                BannerItem body = response.body();
                if (response.isSuccessful()){
                    if (!body.isState()){
                        initializeBanner();
                        ArrayList<Banner> bannerList = new ArrayList<>();
                        BannerAdapter bannerAdapter = new BannerAdapter(bannerList);

                        recycle_banner.setAdapter(bannerAdapter);
                        recycle_banner.setVisibility(View.GONE);
                        progress_banner.setVisibility(View.GONE);
                    } else {
                        Log.d(TAG, "banner data: " + body.getData());
                        initializeBanner();
                        for (int i = 0; i < body.getData().size(); i++){
                            final Banner itemBanner = new Banner();
                            itemBanner.setId(body.getData().get(i).getId());
                            itemBanner.setFoto1(body.getData().get(i).getFoto1());
                            ListBanner.add(itemBanner);
                        }

                        BannerAdapter bannerAdapter = new BannerAdapter(ListBanner);
                        recycle_banner.setAdapter(bannerAdapter);
                        recycle_banner.setVisibility(View.VISIBLE);

                        progress_banner.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<BannerItem> call, Throwable t) {
                Log.d(TAG, "gagal: " + t.toString());
                progress_banner.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Gagal load barang! Coba lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadBarang() {
        progress_belanja.setVisibility(View.VISIBLE);
        Call<BarangItem> barangItem = mobileService.getbarang();
        barangItem.enqueue(new Callback<BarangItem>() {
            @Override
            public void onResponse(Call<BarangItem> call, retrofit2.Response<BarangItem> response) {
                BarangItem body = response.body();
                if (response.isSuccessful()){
                    if (!body.isState()){
                        initializeBarang();
                        ArrayList<Barang> BarangList = new ArrayList<>();
                        MyAdapter myAdapter = new MyAdapter(BarangList);

                        recycle_produk.setAdapter(myAdapter);
                        recycle_produk.setVisibility(View.GONE);
                        progress_belanja.setVisibility(View.GONE);
                    } else {
                        Log.d(TAG, "response data:  " + body.getData());
                        initializeBarang();
                        for (int i = 0; i < body.getData().size(); i++){
                            final Barang itemBarang = new Barang();
                            itemBarang.setFoto(body.getData().get(i).getFoto());
                            itemBarang.setName(body.getData().get(i).getName());
                            itemBarang.setPrice(body.getData().get(i).getPrice());
                            itemBarang.setQty(body.getData().get(i).getQty());
                            ListBarang.add(itemBarang);
                        }

                        MyAdapter myAdapter = new MyAdapter(ListBarang);
                        recycle_produk.setAdapter(myAdapter);
                        recycle_produk.setVisibility(View.VISIBLE);
                        progress_belanja.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<BarangItem> call, Throwable t) {
                Log.d(TAG, "gagal: " + t.toString());
                progress_belanja.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Gagal load barang! Coba lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(() -> {
            initializeBarang();
            initializeBanner();
            loadBarang();
            loadBanner();
            swipe_belanja.setRefreshing(false);
        }, 2000);
    }

    private void initializeBarang() {
        ListBarang = new ArrayList<>();
        MyAdapter myAdapter = new MyAdapter(ListBarang);
        recycle_produk.setAdapter(myAdapter);
        ListBarang.clear();
    }

    private void initializeBanner() {
        ListBanner = new ArrayList<>();
        BannerAdapter bannerAdapter = new BannerAdapter(ListBanner);
        recycle_banner.setAdapter(bannerAdapter);
        ListBanner.clear();
    }


    class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

        ArrayList<Barang> dataItem;

        public MyAdapter(ArrayList<Barang> dataItem) {
            this.dataItem = dataItem;
        }

        @NonNull
        @Override
        public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_produk, parent, false);
            return new MyHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyHolder holder, int position) {
            String fotoBarang = Static.STORAGE_PRODUK + dataItem.get(position).getFoto();
            Log.d(TAG, "foto: " + fotoBarang);

            Picasso.get().load(fotoBarang)
                    .centerCrop()
                    .fit()
                    .placeholder(R.drawable.produk_item)
                    .into(holder.gambar_produk);

            holder.nama_barang.setText(dataItem.get(position).getName());
            holder.stock_barang.setText("Tersisa " + dataItem.get(position).getQty() + " Stok");
            holder.harga_produk.setText("Rp. " + dataItem.get(position).getPrice() + ",-");
        }

        @Override
        public int getItemCount() {
            return dataItem.size();
        }

        class MyHolder extends RecyclerView.ViewHolder {
            TextView nama_barang, stock_barang, harga_produk, lihat_detail;
            ImageView gambar_produk;

            MyHolder(@NonNull View itemView) {
                super(itemView);
                gambar_produk = itemView.findViewById(R.id.item_gambar_produk);
                nama_barang = itemView.findViewById(R.id.item_nama_produk);
                stock_barang = itemView.findViewById(R.id.item_stock_produk);
                harga_produk = itemView.findViewById(R.id.item_harga_produk);
                lihat_detail = itemView.findViewById(R.id.text_lihat_semua_belanja);
            }
        }
    }

    class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.BannerHolder> {

        ArrayList<Banner> bannerItem;

        public BannerAdapter(ArrayList<Banner> bannerItem) {
            this.bannerItem = bannerItem;
        }

        @NonNull
        @Override
        public BannerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_banner, parent, false);
            return new BannerAdapter.BannerHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BannerHolder holder, int position) {
            String fotoBanner = Static.STORAGE_BANNER + bannerItem.get(position).getFoto1();
            Picasso.get().load(fotoBanner)
                    .centerCrop()
                    .placeholder(R.drawable.item_background)
                    .fit()
                    .into(holder.item_banner);
        }

        @Override
        public int getItemCount() {
            return bannerItem.size();
        }

        public class BannerHolder extends RecyclerView.ViewHolder {
            ImageView item_banner;
            public BannerHolder(@NonNull View itemView) {
                super(itemView);
                item_banner = itemView.findViewById(R.id.img_item_banner);
            }
        }
    }

}
package com.struggleteam.mobilerupadi.screens;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.base.BaseActivity;
import com.struggleteam.mobilerupadi.utils.ApiUtils;
import com.struggleteam.mobilerupadi.utils.MobileService;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseActivity {

    private static final int RC_SIGN_IN = 100;
    private GoogleSignInClient googleSignInClient;

    private FirebaseAuth firebaseAuth;
    private static final String TAG = "GOOGLE_SIGN_IN_TAG";

    MaterialButton btnGoogle;
    Button btnMasuk;
    TextInputEditText etEmail, etPassword;
    MobileService mobileService;
    TextView text_daftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mobileService = ApiUtils.MobileService(getApplicationContext());

        btnMasuk = findViewById(R.id.btn_masuk);
        btnGoogle = findViewById(R.id.btn_google_login);
        etEmail = findViewById(R.id.et_email_login);
        etPassword = findViewById(R.id.et_pass_login);
        text_daftar = findViewById(R.id.text_daftar);

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        firebaseAuth = FirebaseAuth.getInstance();

        btnGoogle.setOnClickListener(v -> {
            Log.d(TAG, "onClick: Google SignIn");
            Intent intent = googleSignInClient.getSignInIntent();
            startActivityForResult(intent, RC_SIGN_IN);
        });

        btnMasuk.setOnClickListener(v -> {
            Log.d(TAG, "onClick: User SignIn");
            prosesLogin();
        });

        text_daftar.setOnClickListener(v -> {
            Log.d(TAG, "onClick: Masuk Daftar");
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        });
    }

    private void prosesLogin() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty() && password.isEmpty()){
            showMessage("Kolom masih kosong, Silahkan lengkapi!");
            return;
        }

        if (TextUtils.isEmpty(email)){
            etEmail.setError("Kolom Email masih kosong");
            return;
        }

        if (TextUtils.isEmpty(password)){
            etPassword.setError("Kolom Password masih kosong");
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("email_user", email);
        map.put("password", password);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN){
            Log.d(TAG, "onActivityResult: Google Intent Result");
            Task<GoogleSignInAccount> accountTask = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = accountTask.getResult(ApiException.class);
                firebaseAuthWithGoogleAccount(account);
            }catch (Exception e){
                Log.d(TAG, "onActivityResult: " + e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogleAccount(GoogleSignInAccount account) {
        Log.d(TAG, "firebaseAuthWithGoogleAccount: Firebase Auth With Google Account");
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnSuccessListener(authResult -> {
                    Log.d(TAG, "OnSuccess: Logged In");

                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                    String uid = firebaseUser.getUid();
                    String email = firebaseUser.getEmail();

                    Log.d(TAG, "Firebase User: " + firebaseUser);

                    if (authResult.getAdditionalUserInfo().isNewUser()){
                        Log.d(TAG, "Account Created: " + email);
                        Toast.makeText(this, "Akun dibuat " + email, Toast.LENGTH_LONG).show();
                    }else {
                        Log.d(TAG, "Account Exisiting: " + email);
                        Toast.makeText(this, "Akun sudah ada " + email, Toast.LENGTH_SHORT).show();
                    }

                    startActivity(new Intent(this, MainActivity.class));
                    finish();

                })
                .addOnFailureListener(e -> Log.d(TAG, "OnFailure: Logged Failed " + e.getMessage()));
    }
}
package com.struggleteam.mobilerupadi.screens;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.struggleteam.mobilerupadi.R;

public class OnBoardingActivity extends AppCompatActivity {

    Button btnlanjutkan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        btnlanjutkan = findViewById(R.id.btn_lanjutkan);

        btnlanjutkan.setOnClickListener(v -> {
            startActivity(new Intent(OnBoardingActivity.this, MainActivity.class));
            finish();
        });
    }
}
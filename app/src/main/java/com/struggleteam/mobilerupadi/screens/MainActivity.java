package com.struggleteam.mobilerupadi.screens;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.struggleteam.mobilerupadi.R;
import com.struggleteam.mobilerupadi.adapters.ViewPagerAdapter;
import com.struggleteam.mobilerupadi.screens.belanja.BelanjaFragment;
import com.struggleteam.mobilerupadi.screens.kategori.KategoriFragment;
import com.struggleteam.mobilerupadi.screens.edukasi.EdukasiFragment;
import com.struggleteam.mobilerupadi.screens.keranjang.KeranjangFragment;
import com.struggleteam.mobilerupadi.screens.profil.ProfilFragment;

public class MainActivity extends AppCompatActivity {

    ViewPagerAdapter adapter;
    BottomNavigationView bottomNavigationView;
    ViewPager viewPager;
    MenuItem prevMenuItem;

    boolean doubleBackToExit = false;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.vp1);
        bottomNavigationView = findViewById(R.id.nav_view);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_belanja:
                    viewPager.setCurrentItem(0);
                    break;

                case R.id.navigation_edukasi:
                    viewPager.setCurrentItem(1);
                    break;

                case R.id.navigation_donasi:
                    viewPager.setCurrentItem(2);
                    break;

                case R.id.navigation_keranjang:
                    viewPager.setCurrentItem(3);
                    break;

                case R.id.navigation_profil:
                    viewPager.setCurrentItem(4);
                    break;
            }
            return false;
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page",""+position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter  = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new BelanjaFragment(),"");
        adapter.AddFragment(new EdukasiFragment(),"");
        adapter.AddFragment(new KategoriFragment(),"");
        adapter.AddFragment(new KeranjangFragment(),"");
        adapter.AddFragment(new ProfilFragment(),"");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExit){
            super.onBackPressed();
            return;
        }

        this.doubleBackToExit = true;
        Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(() -> doubleBackToExit = false, 2000);
    }
}
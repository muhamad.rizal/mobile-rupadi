package com.struggleteam.mobilerupadi.utils;

import com.struggleteam.mobilerupadi.models.edukasi.EdukasiItem;
import com.struggleteam.mobilerupadi.models.home.BannerItem;
import com.struggleteam.mobilerupadi.models.home.BarangItem;
import com.struggleteam.mobilerupadi.models.kategori.KategoriItem;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MobileService {

    @GET("user")
    Call<Response> getuser();

    @GET("banner")
    Call<BannerItem> getbanner();

    @GET("barang")
    Call<BarangItem> getbarang();

    @GET("kategori")
    Call<KategoriItem> getkategori();

    @GET("edukasi")
    Call<EdukasiItem> getedukasi();

    @FormUrlEncoded
    @POST("login")
    Call<Response> userlogin(@FieldMap Map<String, String> map);

}

package com.struggleteam.mobilerupadi.utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CovidService {
    @GET("indonesia")
    Call<List<DataIndonesia>> getKasus();
}

package com.struggleteam.mobilerupadi.models.edukasi;

public class Edukasi {

    public Edukasi(){
    }

    private String id;
    private String foto_blog;
    private String judul;
    private String description;
    private String headline;
    private String created_at;

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto_blog() {
        return foto_blog;
    }

    public void setFoto_blog(String foto_blog) {
        this.foto_blog = foto_blog;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}

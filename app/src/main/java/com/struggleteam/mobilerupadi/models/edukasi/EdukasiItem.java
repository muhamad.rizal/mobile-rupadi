package com.struggleteam.mobilerupadi.models.edukasi;

import java.util.List;

public class EdukasiItem {
    List<Edukasi> data;
    boolean state;

    public EdukasiItem(){

    }
    public List<Edukasi> getData() {
        return data;
    }

    public void setData(List<Edukasi> data) {
        this.data = data;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}

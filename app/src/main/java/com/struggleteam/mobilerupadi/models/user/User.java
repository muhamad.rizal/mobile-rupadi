package com.struggleteam.mobilerupadi.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    public User(){

    }

    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("username")
    private String username;
    @Expose
    @SerializedName("nik")
    private String nik;
    @Expose
    @SerializedName("nama_lengkap")
    private String nama_lengkap;
    @Expose
    @SerializedName("email_user")
    private String email_user;
    @Expose
    @SerializedName("phone_user")
    private String phone_user;
    @Expose
    @SerializedName("kecamatan_kec")
    private String kecamatan_kec;
    @Expose
    @SerializedName("desa_ket")
    private String desa_ket;
    @Expose
    @SerializedName("alamat")
    private String alamat;
    @Expose
    @SerializedName("jenis_kelamin")
    private String jenis_kelamin;
    @Expose
    @SerializedName("tempat_lahir")
    private String tempat_lahir;
    @Expose
    @SerializedName("tanggal_lahir")
    private String tanggal_lahir;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getEmail_user() {
        return email_user;
    }

    public void setEmail_user(String email_user) {
        this.email_user = email_user;
    }

    public String getPhone_user() {
        return phone_user;
    }

    public void setPhone_user(String phone_user) {
        this.phone_user = phone_user;
    }

    public String getKecamatan_kec() {
        return kecamatan_kec;
    }

    public void setKecamatan_kec(String kecamatan_kec) {
        this.kecamatan_kec = kecamatan_kec;
    }

    public String getDesa_ket() {
        return desa_ket;
    }

    public void setDesa_ket(String desa_ket) {
        this.desa_ket = desa_ket;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

}

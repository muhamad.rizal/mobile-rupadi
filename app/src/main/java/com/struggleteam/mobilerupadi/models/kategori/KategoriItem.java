package com.struggleteam.mobilerupadi.models.kategori;

import com.struggleteam.mobilerupadi.models.home.Barang;

import java.util.List;

public class KategoriItem {
    List<Kategori> data;
    boolean state;

    public KategoriItem(){

    }
    public List<Kategori> getData() {
        return data;
    }

    public void setData(List<Kategori> data) {
        this.data = data;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}

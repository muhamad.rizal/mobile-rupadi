package com.struggleteam.mobilerupadi.models.home;

import java.util.List;

public class BarangItem {
    List<Barang> data;
    boolean state;

    public BarangItem(){

    }
    public List<Barang> getData() {
        return data;
    }

    public void setData(List<Barang> data) {
        this.data = data;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

}
